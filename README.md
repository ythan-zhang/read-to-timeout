# Read to Timeout

An extension trait for trait [std::io::Read](std::io::Read)

The [std::io::Read](std::io::Read) trait implements many read operations,
but it doesn't contain a simple read method where timeout is the expected
behaviour

This trait provides [read_to_timeout](ReadToTimeout::read_to_timeout) and
[read_to_timeout_or_pattern](ReadToTimeout::read_to_timeout_or_pattern)
that are implemented for all types that implements
[std::io::Read](std::io::Read)

## Usage

### ReadToTimeout::read_to_timeout

[read_to_timeout](ReadToTimeout::read_to_timeout) behaves just
like [read_to_end](std::io::Read::read_to_end), except on timeout, this method
returns `Ok(bytes_read)` instead of `Err(..)`

### ReadToTimeout::read_to_timeout_or_bytes

[read_to_timeout_or_bytes](ReadToTimeout::read_to_timeout_or_bytes) is similar
to [`read_to_timeout`](ReadToTimeout::read_to_timeout)

But when read byte count reaches given `max_byte`, this function returns
`Ok(bytes_read)` immediately

### ReadToTimeout::read_to_timeout_or_pattern

[read_to_timeout_or_pattern](ReadToTimeout::read_to_timeout_or_pattern) is
similar to [`read_to_timeout`](ReadToTimeout::read_to_timeout)

But when a specified pattern is reached, return `Ok(bytes_read)` immediately

#### Note

If the provided buffer is non-empty, while **at least one byte** must be
read before any pattern match, it is possible for pattern to match on
old bytes.
